package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index(Model model) {
        String head = "<!DOCTYPE HTML><html lang=\"en\" xmlns:th=\"http://www.thymeleaf.org\"><head> <meta charset=\"utf-8\"> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\" integrity=\"sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu\" crossorigin=\"anonymous\"> <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css\" integrity=\"sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ\" crossorigin=\"anonymous\"><link href=\"cover.css\" rel=\"stylesheet\"> <title>Hello, World!</title></head><body>";
        
        String body = "<div class=\"site-wrapper\"> <div class=\"site-wrapper-inner\"> <div class=\"cover-container\"> <div class=\"masthead clearfix\"> <div class=\"inner\"> <h3 class=\"masthead-brand\">TEN08</h3> <nav> <ul class=\"nav masthead-nav\"> <li class=\"active\"><a href=\"#\">Home</a></li> <li><a href=\"#\">Features</a></li> <li><a href=\"#\">Contact</a></li> </ul> </nav> </div> </div> <div class=\"inner cover\"> <h1 class=\"cover-heading\">Excellence Built-in.</h1><p class=\"lead\">TEN08 achieves software excellence and provides great software for our customers by reducing the cycle time between having an idea and seeing it in production.</p> <p class=\"lead\"> <a href=\"#\" class=\"btn btn-lg btn-default\">Learn more</a> </p> </div> <div class=\"mastfoot\"> <div class=\"inner\"> <p>Cover template for <a href=\"https://getbootstrap.com/\">Bootstrap</a>, by <a href=\"https://twitter.com/mdo\">@mdo</a>.</p> </div> </div> </div> </div> </div>";
        
        String end = "<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --><script src=\"https://code.jquery.com/jquery-1.12.4.min.js\" integrity=\"sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ\" crossorigin=\"anonymous\"></script><!-- Include all compiled plugins (below), or include individual files as needed --><script src=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\" integrity=\"sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd\" crossorigin=\"anonymous\"></script></body></html>";

        return head + body + end;
    }

}
